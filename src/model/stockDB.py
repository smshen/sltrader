from pymongo import MongoClient

class StockModel(object):

    """model to connect and query to database"""
    uri = 'mongodb://localhost:27017/'
    client = None
    db = None

    def __init__(self):
        """initialize connection of mongodb """

        self.client = MongoClient(self.uri)
        self.db = self.client['stock']

    def getStockDataByDate(self, collectionName='2330', startDate='20000101', endDate='20150101'):
        """get stock data by constraint of start date and end date

        :collectionName: name of queried collection
        :startDate: start date of query
        :endDate: end data of query
        :returns: JSON object of queried stock data 

        """
        
        stock = self.db[collectionName]
        data = stock.find({'date': {'$gte': startDate, '$lte': endDate }})
        return list(data)

        
