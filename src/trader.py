import numpy as np
import pandas as pd
import random
from sklearn import preprocessing

from model.stockDB import StockModel

from keras import backend as K
from keras.callbacks import LearningRateScheduler, Callback

from classifiers.neuralNetwork import constructNetwork
# from classifiers.rnn import constructNetwork

pd.options.mode.chained_assignment = None

def labelAction(price):
    """label the trading action according to the price

    :price: TODO
    :returns: TODO

    """
    if price > 0.2:
        return 1
    elif price < -0.2:
        return -1
    else:
        return 0

def preprocess(rawData, intervalSize=20):
    """clean and convert raw data to numpy array

    :rawData: TODO
    :returns: dataframe of marketData

    """

    rawDF = pd.DataFrame(rawData)
    
    # filter necessary columns
    # priceDF = rawDF[['date', 'close']]
    priceDF = rawDF

    # create column of yesterday price
    priceDF['lag_close']   = priceDF['close'].shift(1)
    priceDF['pct_change'] = priceDF['close'].pct_change()

    priceDF['sumChange'] = priceDF['pct_change'].rolling(window=10).sum() 
    priceDF['label'] = priceDF['sumChange'].apply(labelAction)
    priceDF['label'] = priceDF['label'].shift(-10)


    return priceDF.fillna(0)
    
inputFields = ['amr', 'cdr', 'close', 'dnbs', 'dshr', 'far', 'fcnbs', 'fcr', 'fcshr', 'fmr', 'fvr', 'itnbs', 'itshr', 'lar', 'lcr', 'lfr', 'lmr', 'lvr', 'nbs', 'oriClose', 'p_high', 'p_low', 'p_open', 'pbr', 'per', 'roy', 'shr', 'vol', 'pct_change']

def generateDataPairs(originData):
    """generate training data

    :originData: TODO
    :returns: TODO

    """
    
    # generate normalized input data
    max_abs_scaler = preprocessing.MaxAbsScaler()
    inputList = originData[inputFields].as_matrix()
    inputData = max_abs_scaler.fit_transform(inputList)

    # generate normalized output data
    labelList = originData['label'].as_matrix()
    outputData = np.array(labelList).reshape(len(labelList), 1)

    return inputData, outputData

def lrScheduler(epoch):
    """scheduler for learning rate

    :epoch: TODO
    :returns: TODO

    """
    return 0.5 if epoch > 200000 else 0.01

class scheduler(Callback):

    """scheduler for learning rate and momentum"""

    def __init__(self):
        """TODO: to be defined1. """
        Callback.__init__(self)

    def on_train_begin(self, logs={}):
        """callback on start training

        :logs: TODO
        :returns: TODO

        """
        pass
    def on_batch_end(self, batch, logs={}):
        """callback on the end of each batch

        :batch: TODO
        :logs: TODO
        :returns: TODO

        """
        print batch
        print K.get_value(self.model.optimizer.momentum)
        pass
        
        

def trainModel(inputData, outputData, epoch=1):
    """training the neural network model

    :inputData: TODO
    :outputData: TODO
    :returns: TODO

    """

    nnModel = constructNetwork(len(inputData[0]), len(outputData[0]))

    # change_lr = LearningRateScheduler(lrScheduler)
    cb = scheduler()

    # refactor data for rnn
    # inputData = np.reshape(inputData, (inputData.shape[0], 1, inputData.shape[1]))
    nnModel.fit(inputData, outputData, nb_epoch=epoch, batch_size=100000, callbacks=[cb])
    return nnModel 

def evaluation(model, testData, rawDF):
    """evaluation the performance of the model

    :model: TODO
    :testData: TODO
    :rawData: TODO
    :returns: TODO

    """

    predictionResult = model.predict(testData, verbose=1)
    rawDF['prediction'] = predictionResult

    return rawDF

if __name__ == "__main__":

    dbModel = StockModel()
    queriedData = dbModel.getStockDataByDate('2231', '20100101', '20140101')
    
    # training
    pandasData = preprocess(queriedData)
    inputData, outputData = generateDataPairs(pandasData)

    neuralNetwork = trainModel(inputData, outputData, 5)

    # testing
    testData = dbModel.getStockDataByDate('2231', '20140101', '20160101')
    testPandasData = preprocess(testData)
    testInputData, testOutputData = generateDataPairs(testPandasData)

    # write result to csv file
    result = evaluation(neuralNetwork, testInputData, testPandasData)
    result.to_csv('evaluation-0.5momentum-0.1m-result.csv', encoding='utf-8')
