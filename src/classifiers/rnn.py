from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.recurrent import SimpleRNN, LSTM, GRU
from keras.optimizers import RMSprop
import numpy as np

def constructNetwork(input_dim=4, output_dim=3):
    """construct a model of neural network by keras

    :input_dim: TODO
    :output_dim: TODO
    :returns: TODO

    """
    # for a single-input model with 2 classes (binary):
    model = Sequential()

    # add input layer
    model.add(SimpleRNN( 2 * input_dim, input_dim=input_dim, input_length=1, activation='tanh'))

    model.add(Dense( output_dim, activation='tanh'))

    # add hidden layer
    model.add(SimpleRNN( input_dim, activation='tanh'))

    # # add hidden layer
    model.add(SimpleRNN( input_dim, activation='tanh'))


    # add output layer
    model.add(Dense( output_dim, activation='tanh'))


    model.compile(optimizer='sgd',
                  loss='mse',
                  metrics=['accuracy'])
    
    return model
